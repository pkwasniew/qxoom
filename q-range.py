'''
Created on 3 janv. 2018

@author: Kwasniewski
'''

import pyFAI
import pylab as p
import fabio
from pyFAI.calibrant import get_calibrant
from psutil._common import sdiskio
from dask.array.routines import diff
from matplotlib.patches import Ellipse

# Get all detectors form pyFAI
all_detectors = list(set(pyFAI.detectors.ALL_DETECTORS.values()))
all_detectors.sort(key=lambda i:i.__name__)
nb_det = len(all_detectors)

# print("Number of detectors registered: %i with %i unique detectors"%(len(pyFAI.detectors.ALL_DETECTORS),nb_det))
# print()
# print("List of all supported detectors")
# for i in all_detectors:
#     print(i())
    
wavelength = {"CuKa12": 1.54189e-10,
              "CuKa1": 1.5405929759615018e-10,
               "MoKa1": 0.7093171e-10,
    }


# Choose detector
detector = pyFAI.detector_factory("Pilatus_300k")
#detector = pyFAI.detector_factory("Eiger_1M")
dim1,dim2 = detector.shape
psize = detector.pixel1 # pixels are square

detector_config = {"center": {"cen1" : dim1/2 + 2e-3/psize,
                              "cen2" : dim2/2 - 25e-3/psize},
                    "offset1": {"cen1" : dim1/2 - 49.25e-3/psize,
                               "cen2" : dim2/2 - 25e-3/psize},
                    "offset2": {"cen1" : dim1/2 - 60.75e-3/psize,
                               "cen2" : dim2/2 - 25e-3/psize},
    }

print(detector_config)

sdi = 0.065 #  initial sample-detector distance in [m]
sdf = 2.5 #  final sample-detector distance in [m]
sd_dif = sdf - sdi

# Define measurement distances
dist = {#"WAXS": sdi,
        "WAXS standard": 0.270,
        #"WAXS off1": sdi,
        "WAXS standard off1": 0.270,
        #"MAXS": sdi + sd_dif/4,
        #"SAXS": sdi + 3*sd_dif/4,
        #"ESAXS 0": sdf - 0.258 + sdi,
        "SAXS": 1.5,
        "ESAXS 1": sdf
        }

# For all measurement distances calculate the q range for 
# a chosen detector PONI

def calc_q_range(detector, wavelength, distance, cen1, cen2):
    poni1 = cen1 * psize
    poni2 = cen2 * psize
    mask = detector.calc_mask()
    ai = pyFAI.AzimuthalIntegrator(dist=distance, detector=detector)
    ai.wavelength = wavelength
    ai.poni1 = poni1
    ai.poni2 = poni2
    # Show simulated calibration data
    cal = get_calibrant("AgBh")
    cal.wavelength = wavelength
    img = cal.fake_calibration_image(ai)
    res1 = ai.integrate1d(img, 1024, unit="q_nm^-1", mask=mask)
    qArray = ai.qArray()
    qArrayMasked = p.ma.masked_array(qArray,mask)
    flat_data = qArrayMasked.compressed().flatten()
    return flat_data,res1

def alt(i):
    if i % 2:
        return 1
    else:
        return -1  

if __name__ == "__main__":
    # Choose wavelength to use in the calculation
    wl = wavelength["CuKa12"]
    chosen_config = 'center'
    cen1, cen2 = detector_config[chosen_config].values()
    
      
    # Calculate and show q histograms
    fig1 = p.figure()
    ax1 = fig1.add_subplot(111)
    data = []
    sim_data = []
    for key in dist.keys():
        if "off1" in key:
            chosen_config = 'offset1' 
        else:
            chosen_config = 'center'
        cen1, cen2 = detector_config[chosen_config].values()
        q_range, res_sim = calc_q_range(detector, wl, dist[key], cen1, cen2)
        #dq = 4*p.pi*p.sin(0.5*p.arctan(detector.pixel1/dist[key]))/(wl*1e10)
        dq = (2*p.pi*detector.pixel1/dist[key])/(wl*1e10)
        print('Distance: %.2f m; dq: %.2e 1/A;' % (dist[key],dq))
        data.append(q_range)
        sim_data.append(res_sim)
    labels = list(dist.keys())
    for i in range(len(labels)):
        item = labels[i]
        curr_dist = dist[item]
        labels[i] = '%s, %.2f mm' % (item, curr_dist * 1e3)
        
    res = p.hist(data, bins=2048, normed=False,
                       histtype=u'step', label=labels)
    p.legend()
    ax1.set_yscale("log")
    ax1.set_xlabel(r'$q\:[nm^{-1}]$')
    ax1.set_ylabel('Number of pixels')
    # Annotate q histogram
    th = 20
    ax1.axhline(th,color='grey')
    for i in range(len(res[0])):
        n = res[0][i]
        bins = res[1]
        patch = res[2][i][0]
        color = patch.get_ec()
        filtered_n = p.where(n>=th)
        try:
            n_l = filtered_n[0][0]
            n_r = filtered_n[0][-1]
            ann = ax1.annotate(r'%.2f $[nm^{-1}]$' % bins[n_r],
                  xy=(bins[n_r], n[n_r]), xycoords='data',
                  xytext=(-40, -i * 20 + 40), textcoords='offset pixels',
                  size=10, va="center",
                  bbox=dict(boxstyle="round", fc=color, ec="none"),
                  arrowprops=dict(arrowstyle="wedge,tail_width=1.",
                                  fc=color, ec="none",
                                  patchA=None,
                                  patchB=None,
                                  relpos=(0.2, 0.5)))
            #ax1.axvline(bins[n_l],color=color)
            #ax1.axvline(bins[n_r],color=color)
        except IndexError:
            pass
        
    
    # Plot the simulated data
    fig2 = p.figure()
    ax2 = fig2.add_subplot(111)
    for i in range(len(sim_data)):
        q,I = sim_data[i]
        ax2.plot(q,I)
    p.show()