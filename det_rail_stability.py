'''
Created on 4 janv. 2018

@author: Kwasniewski
'''
import fabio
from scipy import optimize
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
from pylab import *
import pandas as pd
from sympy.polys.tests import test_subresultants_qq_zz

# Helper functions

def gaussian(height, center_x, center_y, width_x, width_y):
    """Returns a gaussian function with the given parameters"""
    width_x = float(width_x)
    width_y = float(width_y)
    return lambda x,y: height*np.exp(
                -(((center_x-x)/width_x)**2+((center_y-y)/width_y)**2)/2)

def moments(data):
    """Returns (height, x, y, width_x, width_y)
    the gaussian parameters of a 2D distribution by calculating its
    moments """
    total = data.sum()
    X, Y = np.indices(data.shape)
    x = (X*data).sum()/total
    y = (Y*data).sum()/total
    col = data[:, int(y)]
    width_x = np.sqrt(abs((np.arange(col.size)-y)**2*col).sum()/col.sum())
    row = data[int(x), :]
    width_y = np.sqrt(abs((np.arange(row.size)-x)**2*row).sum()/row.sum())
    height = data.max()
    return height, x, y, width_x, width_y
    
def fitGaussian(data):
    """Returns (height, x, y, width_x, width_y)
    the gaussian parameters of a 2D distribution found by a fit"""
    params = moments(data)
    errorfunction = lambda p: np.ravel(gaussian(*p)(*np.indices(data.shape)) -
                                 data)
    p, success = optimize.leastsq(errorfunction, params)
    return p

def printStatistics(data,label,unit):
    data_mean = data.mean()
    data_std = data.std()
    data_ptp = data.ptp()
    print('%s mean: %.2f %s' % (label,data_mean,unit))
    print('%s std dev: %.2f %s' % (label,data_std,unit))
    print('%s peak-to-peak: %.2f %s' % (label,data_ptp,unit))
    

def _f( x, p0, p1):
    return p0*x + p1

def _ff(x, p):
    return _f(x, *p)

def fitLeastSq(p0, datax, datay, function):

    errfunc = lambda p, x, y: function(x,p) - y

    pfit, pcov, infodict, errmsg, success = optimize.leastsq(errfunc, p0, args=(datax, datay),full_output=1, epsfcn=0.0001)

    if (len(datay) > len(p0)) and pcov is not None:
        s_sq = (errfunc(pfit, datax, datay)**2).sum()/(len(datay)-len(p0))
        pcov = pcov * s_sq
    else:
        pcov = np.inf

    error = [] 
    for i in range(len(pfit)):
        try:
            error.append(np.absolute(pcov[i][i])**0.5)
        except:
            error.append( 0.00 )
    pfit_leastsq = pfit
    perr_leastsq = np.array(error) 
    return pfit_leastsq, perr_leastsq

def axisEqual3D(ax):
    extents = np.array([getattr(ax, 'get_{}lim'.format(dim))() for dim in 'xyz'])
    sz = extents[:,1] - extents[:,0]
    centers = np.mean(extents, axis=1)
    maxsize = max(abs(sz))
    r = maxsize/2
    for ctr, dim in zip(centers, 'xyz'):
        getattr(ax, 'set_{}lim'.format(dim))(ctr - r, ctr + r)
        
def fitDivergence(data):
    '''Fit the beam divergence from the change of Gaussian width of the beam
    along the detector track.
    '''
    result = {}
    # Fit divergence in x direction
    y = data.as_matrix(columns=['w1 [m]']).flatten()# Gaussian width in direction 1 in [m]
    dist_a = data.as_matrix(columns=['dist [m]']).flatten() 
    p0 = (y[-1] - y[0])/(dist_a[-1] - dist_a[0]) # guess for the slope
    p1 = y[0] - p0 * dist_a[0] # guess for the intercept
    pstart = (p0,p1)
    pfitx, perrx = fitLeastSq(pstart, dist_a, y, _ff)
    result['slope1'] = pfitx[0]
    result['slope1_err'] = perrx[0]
    result['intercept1'] = pfitx[1]
    result['intercept1_err'] = perrx[1]
    
    #fit_x = _ff(dist_a,pfitx)
    #ax21.plot(dist_a,fit_x,'-r',label='linear fit')
    #print("\n# Fit parameters and parameter errors from lestsq method :")
    #print("pfit = ", pfitx)
    #print("perr = ", perrx)
    
    # Fit divergence in y direction
    y = data.as_matrix(columns=['w2 [m]']).flatten()
    p0 = (y[-1] - y[0])/(dist_a[-1] - dist_a[0]) # guess for the slope
    p1 = y[0] - p0 * dist_a[0] # guess for the intercept
    pstart = (p0,p1)
    pfity, perry = fitLeastSq(pstart, dist_a, y, _ff)
    result['slope2'] = pfity[0]
    result['slope2_err'] = perry[0]
    result['intercept2'] = pfity[1]
    result['intercept2_err'] = perry[1]
    #fit_y = _ff(dist_a,pfity)
    #ax22.plot(dist_a,fit_y,'-r',label='linear fit')
    #print("\n# Fit parameters and parameter errors from lestsq method :")
    #print("pfit = ", pfity)
    #print("perr = ", perry)
    div_x = (2 * arctan(pfitx[0])* 1e3,2 * arctan(perrx[0])* 1e3) 
    div_y = (2 * arctan(pfity[0])* 1e3,2 * arctan(perry[0])* 1e3)
    result['div1 [mrad]'] = div_x[0]
    result['div1_err [mrad]'] = div_x[1]
    result['div2 [mrad]'] = div_y[0]
    result['div2_err [mrad]'] = div_y[1]
    return result
    
def analyse(data_files,roi):
        '''Analyse a series of data frames
        
        Parameters:
        data_files - list of data files to process
        roi - region of interest to be analysed [x0,x1,y0,y1]
        '''
        n = len(data_files) 
        cen_x = np.zeros(n) # center coordinate 1
        cen_y = np.zeros(n) # center coordinate 2
        wx_a = np.zeros(n) # Gaussian width along coord. 1
        wy_a = np.zeros(n) # Gaussian width along coord. 2
        dist_a = np.zeros(n) # detector distance
        x0,x1,y0,y1 = roi
        results = []
        c = 0
        for i in range(len(data_files)):
                fname = data_files[i]
                frame = fabio.open(fname)
                npimg = frame.data
                header = frame.header
                psize = float(header['PSize_1'])
                img_roi = npimg[x0:x1,y0:y1]
                params = fitGaussian(img_roi)
                cen_x[c] = params[1]
                cen_y[c] = params[2]
                wx_a[c] = params[3]
                wy_a[c] = params[4]
                dist_a[c] = float(header['dety']) # relative distance in [mm]
                # append the results container keeping [m] as distance units
                results.append({'cen1 [m]': (params[1] + x0) * psize,
                                'cen2 [m]': (params[2] + y0) * psize,
                                'w1 [m]': abs(params[3]) * psize,
                                'w2 [m]': abs(params[4]) * psize,
                                'dist [m]': float(header['dety']) * 1e-3})
                #print params
                c += 1
        results = pd.DataFrame(results)
        return results


def findROI(img,radius):
    '''Return a ROI with the defined radius around the diret beam
    '''
    c1,c2 = where(img >= img.max())
    x0 = c1[0] - radius
    x1 = c1[0] + radius
    y0 = c2[0] - radius
    y1 = c2[0] + radius
    return [x0,x1,y0,y1]
        
def plotBeamPos3D(data,ax=None):
    '''Plot the beam position along the detector track
    '''
    # Unpack the data for convenience 
    cen_x = data.as_matrix(columns=['cen1 [m]']) * 1e6 # convert units to [um]
    cen_y = data.as_matrix(columns=['cen2 [m]']) * 1e6 # convert units to [um]
    dist_a = data.as_matrix(columns=['dist [m]']) * 1e3 # convert units to [mm]
    
    # Plot the data in a 3d scatter plot
    if ax == None:
        fig = figure(figsize=(9,6))
        fig.set_facecolor('white')
        ax = fig.add_subplot(111, projection='3d')
    cmx = cm.get_cmap('viridis')
    pic = ax.scatter(cen_x - cen_x.mean(), dist_a, cen_y - cen_y.mean(), c=dist_a.flatten(),cmap=cmx)
    colorbar(pic,ax=ax)
    ax.set_ylabel(r'Y $[mm]$')
    ax.set_xlabel(r'X $[\mu m]$')
    ax.set_zlabel(r'Z $[\mu m]$')
    axisEqual3D(ax)
    ax.set_xlim(-150,200)
    ax.set_zlim(-150,200)
    ax.view_init(20,-60)
    
def plotBeamPos2D(data,axL=None,axR=None):
    cen_x = data.as_matrix(columns=['cen1 [m]']) * 1e6 # convert units to [um]
    cen_y = data.as_matrix(columns=['cen2 [m]']) * 1e6 # convert units to [um]
    dist_a = data.as_matrix(columns=['dist [m]']) * 1e3 # convert units to [mm]
    if (axL == None) & (axR == None):
        fig = figure(figsize=(8,4))
        axL = fig.add_subplot(121)
        axR = fig.add_subplot(122)
    axL.set_title(r'X $[\mu m]$')
    axR.set_title(r'Z $[\mu m]$')
    axL.set_xlabel('dety [mm]')
    axR.set_xlabel('dety [mm]')
    axL.plot(dist_a, cen_x - cen_x.mean())
    axR.plot(dist_a, cen_y - cen_y.mean())
    tight_layout()

def plotBeamDivergence(data,fitRes,axL=None,axR=None):
    w1 = data.as_matrix(columns=['w1 [m]']).flatten()# Gaussian width in direction 1 in [m]
    w2 = data.as_matrix(columns=['w2 [m]']).flatten()# Gaussian width in direction 2 in [m]
    dist_a = data.as_matrix(columns=['dist [m]']).flatten()
    if (axL == None) & (axR == None):
        fig = figure(figsize=(8,4))
        axL = fig.add_subplot(121)
        axR = fig.add_subplot(122)
    axL.set_title(r'X')
    axR.set_title(r'Z')
    axL.set_ylabel('Beam size [m]')
    axL.set_xlabel('dety [m]')
    axR.set_xlabel('dety [m]')
    axL.plot(dist_a,w1,label='data')
    axR.plot(dist_a,w2,label='data')
    fit_w1 = _ff(dist_a,[fitRes['slope1'],fitRes['intercept1']])
    axL.plot(dist_a,fit_w1,'-r',label='fit')
    fit_w2 = _ff(dist_a,[fitRes['slope2'],fitRes['intercept2']])
    axR.plot(dist_a,fit_w2,'-r',label='fit')
    legend()
    text(0.95, 0.05, """
    div. x : $%.2f \pm %.4f$ mrad
    """ %(fitRes['div1 [mrad]'],fitRes['div1_err [mrad]']),
            fontsize=8, horizontalalignment='right',
            verticalalignment='bottom', transform=axL.transAxes)
    text(0.95, 0.05, """
    div. z : $%.2f \pm %.4f$ mrad
    """ %(fitRes['div2 [mrad]'],fitRes['div2_err [mrad]']),
            fontsize=8, horizontalalignment='right',
                verticalalignment='bottom', transform=axR.transAxes)
    tight_layout()

def saveData(data,fname):
    '''Saves the given pandas DataFrame to a csv file and prints a confirmation
    '''
    data.to_csv(fname,sep=';')
    print('Data saved to %s' % fname)
    
    
def main():
    #data_dir = "C:\\Users\\kwasniewski\\OneDrive - XENOCS\\Documents\\Projects\\Q-xoom\\dety_test\\"
    data_dir = "C:\\Users\\kwasniewski\\OneDrive - XENOCS\\Documents\\Projects\\Q-xoom\\KIT\\test\\"
    fn0 = 308#5#990#763#535#307
    fn1 = 364#242#1217#990#763#535
    data_files = [data_dir + 'test_0_%05d.edf' % i for i in range(fn0,fn1)]
    frame0 = fabio.open(data_files[0])
    img0 = frame0.data
    date = frame0.header['Date']
    date = date.replace(':','-')
    resFileHeader = 'dety_analysis_%s.csv' % (date.replace(' ','_'))
    roi = findROI(img0,50)
    data = analyse(data_files,roi)
    plotBeamPos3D(data)
    divergence = fitDivergence(data)
    plotBeamPos2D(data)
    plotBeamDivergence(data,divergence)
    fname = data_dir+resFileHeader
    saveData(data,fname)
    show()


if __name__ == "__main__":
    main()

